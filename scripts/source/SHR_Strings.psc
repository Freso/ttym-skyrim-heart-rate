Scriptname SHR_Strings extends quest

Globalvariable property SHR_HeartRate Auto
GlobalVariable Property SHR_LTS Auto

				;Beta 1.2

			;Config Menu

		;Page "General"
	
		string function PageNameGeneral() Global
			return "General"
		endfunction
	
	;Enable / Disable
	
string function HeaderEnableDisable() Global
	return "Enable / Disable"
endfunction

string function TOEnableName() Global
	return "Enable Skyrim Heart Rate"
endfunction

string function TOLTSName() Global
	return "Enable Long Term Stamina"
endfunction

	;Feedback: (Check LTS)
	
string function HeaderLTSFB() Global
	return "Feedback: (Check LTS)"
endfunction

string function TOEnableImmersiveLTSName() Global
	return "Enable Immersive LTS"
endfunction	

	;Presets

string function HeaderPresets() Global
	return "Presets"
endfunction

string function TexOPresetDefaultName() Global
	return "Default"
endfunction

string function TexOPresetMageName() Global
	return "Mage"
endfunction

string function TexOPresetWarriorName() Global
	return "Warrior"
endfunction

string function TexOPresetRangerName() Global
	return "Ranger"
endfunction

string function TexOPresetIJWRName() Global
	return "I Just Wanna Run"
endfunction

	;Feedback (Check Pulse)

string function HeaderCheckPulse() Global
	return "Feedback (Check Pulse)"
endfunction

string function TOSoundName() Global
	return "Enable sound feedback"
endfunction

string function TOMessageName() Global
	return "Enable message feedback"
endfunction

	;Hotkeys

string function HeaderHotkeys() Global
	return "Hotkeys"
endfunction

string function KORelaxKeyName() Global
	return "Relax Hotkey"
endfunction

string function KOCheckPulseKeyName() Global
	return "Check Pulse Hotkey"
endfunction

string function KOCheckLTSKeyName() Global
	return "Check LTS Hotkey"
endfunction

		;Page "Advanced"
		
		string function PageNameAdvanced() Global
			return "Advanced"
		endfunction
		
	;Long Term Stamina

String Function	LTSHeader() Global
	return "Long Term Stamina"
endfunction

string function MOLTSModelName() Global
	return "Long Term Stamina model"
endfunction

	string function LTSModelsQ() Global
		Return "Quadratic"
	endfunction

	string function LTSModelsL() Global
		return "Linear"
	endfunction
	
	string function LTSModelsC() Global
		return "Constant"
	endfunction

string function SOLTSMultiplierName() Global
	return "Select multiplier"
endfunction

string function SOIDLTSRatioName() Global
	return "Increase / decrease ratio"
endfunction

string function SOLTSThresholdName() Global
	return "Select threshold"
endfunction

	;Heart Rate

String Function HeartRateHeader() Global
	return "Heart Rate"
endfunction

string function MOHeartRateModelName() Global
	return "Heart Rate model"
endfunction

	string function HRModelsQ() Global
		return "Quadratic"
	endfunction
	
	string function HRModelsL() Global
		return "Linear"
	endfunction
	
	string function HRModelsC() Global
		return "Constant"
	endfunction

string function SOHRMultiplierName() Global
	return "Select multiplier"
endfunction

string function SOIDHRRatioName() Global
	return "Increase / decrease ratio"
endfunction

string function SOSprintCapName() Global
	return "Select sprint cap"
endfunction

string function SORunCapName() Global
	return "Select run cap"
endfunction

string function SOCombatCapName() Global
	return "Select combat cap"
endfunction

string function TOEnableAdrenalineRushName() Global
	return "Enable adrenaline rush"
endfunction

		;Infotext - this is the text you see when you highlight options in the MCM
		;\n simply means new line, so you should use that, if you see your description getting too long
		
String function TOEnableDesc() Global
	return "Toggles Skyrim Heart Rate on and off."
endfunction
	
string function TOLTSDesc() Global
	return "Toggles Long Term Stamina on and off.\nLTS is the concept of getting tired after a long period of execise"
endfunction
	
string function TOSoundfbDesc() Global
	return "Let's you hear your heartbeat when you check your pulse.\nAdds to immersion"
endfunction
	
string function tomessagefbDesc() Global
	return "Gives you a precise read on your heart rate, when you check your pulse.\nBreaks immersion"
endfunction
	
string function toenableadrenalinerushDesc() Global
	return "Makes the player's heart rate jump about 15 BPM, when entereing combat.\nGives a quick boost to heart rate, when it's needed."
endfunction
	
string function soltsthresholdDesc() Global
	return "Choose how high your heart rate can go,\nbefore it starts having a negative influence on your long term stamina."
endfunction
	
string function soltsmultiplierDesc() Global
	return "A simple multiplier, deciding how fast you want your LTS to change"
endfunction
	
string function sohrmultiplierDesc() Global
	return "A simple multiplier, deciding how fast you want your heart rate to change"
endfunction
	
string function sosprintcapDesc() Global
	return "Chooses where your heart rate stops changing, if you sprint"
endfunction
	
string function soruncapDesc() Global
	return "Chooses where your heart rate stops changing, if you run"
endfunction
	
string function socombatcapDesc() Global
	return "Chooses where your heart rate stops changing, if you are in combat"
endfunction
	
string function moheartratemodelDesc() Global
	return "See technical description for explanation"
endfunction
	
string function moltsmodelDesc() Global
	return "See technical description for explanation"
endfunction
	
string function korelaxkeyDesc() Global
	return "Hotkey for relaxing.\nThis needs to be held down for a second"
endfunction
	
string function kocheckpulsekeyDesc() Global
	return "Hotkey for checking your pulse"
endfunction
	
string function kocheckltskeyDesc() Global
	return "Hotkey for checking LTS"
endfunction
	
string function TOEnableImmersiveLTSDesc() Global
	return "Gives you a description of how you're feeling.\nOpposed to a simple numeric readout.\nAdds to immersion"
endfunction
	
string function SOIDHRRatioDesc() Global
	return "Decides the increase / decrease ratio of your heart rate.\nThe higher this is, the faster your heart rate will rise\ncompared to how fast it will fall"
endfunction	
	
string function SOIDLTSRatioDesc() Global
	return "Decides the increase / decrease ratio of your LTS.\nThe higher this is, the faster your LTS will rise\ncompared to how fast it will fall"
endfunction
	

			;Notifications
			
String Function CheckPulseNotif() Global
	return "My heart is beating at "
endfunction

string function CheckLTSNotif00() Global
	return "My LTS is at "
endfunction

	string function CheckLTSNotif01() Global
		return "My body is screaming for rest!"
	endfunction

	string function CheckLTSNotif02() Global
		return "I'm feeling completely exhausted."
	endfunction

	string Function CheckLTSNotif03() Global
		return "I'm feeling exhausted."
	endfunction

	string Function CheckLTSNotif04() Global
		return "I'm feeling very tired."
	endfunction

	string Function CheckLTSNotif05() Global
		return "I'm feeling tired."
	endfunction

	string Function CheckLTSNotif06() Global
		return "I'm feeling a bit tired."
	endfunction

	string Function CheckLTSNotif07() Global
		return "I'm feeling rested."
	endfunction

	string Function CheckLTSNotif08() Global
		return "I'm feeling moderately fresh."
	endfunction

	string Function CheckLTSNotif09() Global
		return "I'm feeling fresh."
	endfunction

	string Function CheckLTSNotif10() Global
		return "I'm feeling fresh, and eager to fight!"
	endfunction

String function Relaxfail() Global
	return "I cannot relax now! I need to find a place I can sit down."
endfunction

String function RelaxSuccess() Global
	return "I'm relaxed."
endfunction

;That is all!