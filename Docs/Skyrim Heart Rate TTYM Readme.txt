﻿This is a small and simple "patch" for Skyrim Heart Rate[1] to convert its in-game,
3rd person message into 1st person messages à la Think To Yourself Messages[2].

Note that you don't need to install TTYM to use this, but it doesn't make much sense without having SHR. If NMM asks to overwrite, say yes. (It should overwrite the SHR_Strings.* in the scripts/ directory. If it's trying to overwrite anything else... something messed up.)

That's all, really.

[1] Skyrim Heart Rate: http://skyrim.nexusmods.com/mods/35050
[2] Think To Yourself: http://skyrim.nexusmods.com/mods/32060